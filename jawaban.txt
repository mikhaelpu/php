C:\xampp\mysql\bin
λ mysql -uroot

Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database
categories
create table categories ( id int(8) auto_increment, nama varchar(255), primary key(id) );

items
create table items(
    -> id int(8) auto_increment,
    -> nama varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

users
create table users(
    -> id int (8) auto_increment,
    -> nama varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

Soal 3 Memasukkan Data pada Table
categories
 insert into categories(nama) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

users
 insert into users (nama, email, password) values
    -> ("John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");

items
 insert into items (nama, description, price, stock, category_id) values
    -> ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh"," baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Wathch","jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database
a. Mengambil data users
select id, nama, email from users ;

b. Mengambil data items
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
select * from items where price > 1000000;

Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
select * from items where nama like 'Uniklo%';

c. Menampilkan data items join dengan kategori
Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items. Berikut contoh tampilan data yang ingin didapatkan
select items.nama, items.description, items.price, items.stock, items.category_id, categories.nama from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000.
update items set price = 2500000 where id = 1;